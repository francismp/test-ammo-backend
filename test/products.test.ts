import request from 'supertest';
import app from '../src/server';

describe('Test products path response', () => {

  test('It should response 404 for a invalid path', (done: any) => {
    return request(app).get('/testtest').then((response: any) => {
      expect(response.statusCode).toBe(404);
      done();
    });
  });

  test('It should response the GET request', (done: any) => {
    return request(app).get('/products').then((response: any) => {
      const { body } = response;
      expect(body.rows).toBeDefined();
      expect(response.statusCode).toBe(200);
      done();
    });
  });

  test('It should response the query params', (done: any) => {
    return request(app).get('/products?limit=10&offset=10').then((response: any) => {
      const { body } = response;
      expect(body.limit).toEqual('10');
      expect(body.offset).toEqual('10');
      expect(response.statusCode).toBe(200);
      done();
    });
  });

  test('It should limit its response', (done: any) => {
    return request(app).get('/products?limit=2&offset=0').then((response: any) => {
      const { body } = response;

      expect(body.limit).toEqual('2');
      expect(body.rows.length).toEqual(2)

      expect(response.statusCode).toBe(200);
      done();
    });
  });

  test('It should response the products with categories and images', (done: any) => {
    return request(app).get('/products?limit=10&offset=0').then((response: any) => {
      const { body } = response;

      expect(body.limit).toEqual('10');
      expect(body.offset).toEqual('0');

      expect(body.rows[0].categories).toBeDefined()
      expect(body.rows[0].images).toBeDefined()
      expect(response.statusCode).toBe(200);
      done();
    });
  });

  test('It should response the products with properties', (done: any) => {
    return request(app).get('/products?limit=10&offset=0').then((response: any) => {
      const { body } = response;

      expect(body.limit).toEqual('10');
      expect(body.offset).toEqual('0');

      expect(body.rows[0].name).toBeDefined()
      expect(body.rows[0].price).toBeDefined()
      expect(body.rows[0].old_price).toBeDefined()
      expect(response.statusCode).toBe(200);
      done();
    });
  });

  test('It should response the product categories with properties', (done: any) => {
    return request(app).get('/products?limit=10&offset=0').then((response: any) => {
      const { body } = response;

      expect(body.rows[0].categories[0].name).toBeDefined()
      expect(body.rows[0].categories[0].description).toBeDefined()

      done();
    });
  });

  test('It should response the product images with properties', (done: any) => {
    return request(app).get('/products?limit=10&offset=0').then((response: any) => {
      const { body } = response;

      expect(body.rows[0].images[0].name).toBeDefined()
      expect(body.rows[0].images[0].path).toBeDefined()

      done();
    });
  });
})