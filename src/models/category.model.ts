import {
  Table,
  Column,
  Model,
  PrimaryKey,
  NotNull,
  DataType,
  BelongsToMany,
  BeforeCreate,
  BeforeUpdate,
  AutoIncrement
} from 'sequelize-typescript';

import { Product } from './product.model';
import { ProductCategory } from './product-category.model';

@Table({
  tableName: 'categories',
  timestamps: true,
  createdAt: 'created_at',
  updatedAt: 'updated_at'
})
export class Category extends Model<Category>{

  @AutoIncrement
  @PrimaryKey
  @Column
  id: number;

  @Column
  name: string;

  @Column({ type: DataType.TEXT })
  description: string;

  @BelongsToMany(() => Product, () => ProductCategory)
  prodcuts: Product[];

  created_at: Date;
  updated_at: Date;

  @BeforeCreate
  static touchTimestamps(instance: Category) {
    instance.created_at = new Date();
    instance.updated_at = new Date();
  }

  @BeforeUpdate
  static touchUpdatedAt(instance: Category) {
    instance.updated_at = new Date();
  }
}
