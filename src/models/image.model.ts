import {
  Table,
  Column,
  Model,
  PrimaryKey,
  NotNull,
  BeforeCreate,
  BeforeUpdate,
  AutoIncrement
} from 'sequelize-typescript';

@Table({
  tableName: 'images',
  timestamps: true,
  createdAt: 'created_at',
  updatedAt: 'updated_at'
})
export class Image extends Model<Image>{

  @AutoIncrement
  @PrimaryKey
  @Column
  id: number;

  @Column
  name: string;

  @Column
  path: string;

  created_at: Date;
  updated_at: Date;

  @BeforeCreate
  static touchTimestamps(instance: Image) {
    instance.created_at = new Date();
    instance.updated_at = new Date();
  }

  @BeforeUpdate
  static touchUpdatedAt(instance: Image) {
    instance.updated_at = new Date();
  }
}
