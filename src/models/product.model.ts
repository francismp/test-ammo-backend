import {
  Table,
  Column,
  Model,
  PrimaryKey,
  NotNull,
  DataType,
  Unique,
  BelongsToMany,
  Scopes,
  BeforeCreate,
  BeforeUpdate,
  AutoIncrement
} from 'sequelize-typescript';

import { Image } from './image.model';
import { Category } from './category.model';
import { ProductImage } from './product-image.model';
import { ProductCategory } from './product-category.model';

@Scopes({
  paginated: (term: string, limit: number = 10, offset: number = 0) => {
    return {
      where: {
        name: {
          $like: `%${term}%`
        }
      },
      limit: limit,
      offset: offset
    }
  }
})
@Table({
  freezeTableName: true,
  tableName: 'products',
  timestamps: true,
  createdAt: 'created_at',
  updatedAt: 'updated_at'
})
export class Product extends Model<Product>{

  @AutoIncrement
  @PrimaryKey
  @Column
  id: number;

  @Unique
  @Column
  sku: string;

  @Unique
  @Column
  name: string;

  @Column({ type: DataType.FLOAT })
  price: number;

  @Column({ type: DataType.FLOAT })
  old_price: number;

  @Column({ type: DataType.TEXT })
  description: string;

  @BelongsToMany(() => Category, () => ProductCategory)
  categories?: Category[];

  @BelongsToMany(() => Image, () => ProductImage)
  images?: Image[];

  created_at: Date;
  updated_at: Date;

  @BeforeCreate
  static touchTimestamps(instance: Product) {
    instance.created_at = new Date();
    instance.updated_at = new Date();
  }

  @BeforeUpdate
  static touchUpdatedAt(instance: Product) {
    instance.updated_at = new Date();
  }
}
