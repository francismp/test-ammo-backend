import {
  Table,
  Column,
  Model,
  ForeignKey
} from 'sequelize-typescript';

import { Product } from './product.model';
import { Category } from './category.model';

@Table({tableName: 'product_has_category'})
export class ProductCategory extends Model<ProductCategory>{

  @Column
  @ForeignKey(() => Category)
  category_id: number;

  @Column
  @ForeignKey(() => Product)
  product_id: number;
}