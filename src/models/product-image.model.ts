import { Model, Table, Column, ForeignKey } from 'sequelize-typescript';

import { Image } from './image.model';
import { Product } from './product.model';

@Table({ tableName: 'product_has_image' })
export class ProductImage extends Model<ProductImage> {

  @Column
  @ForeignKey(() => Image)
  image_id: number;

  @Column
  @ForeignKey(() => Product)
  product_id: number;
}