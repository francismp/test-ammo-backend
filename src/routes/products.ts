import express from 'express';
import ProductController from '../controllers/product';

const router = express.Router();

router.get('/products', ProductController.paginatedSearch);

export default router;
