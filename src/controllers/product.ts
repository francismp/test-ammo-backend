import { Request, Response } from 'express';
import { Product, Category, Image } from '../models';

class ProductController {

  async paginatedSearch(req: Request, res: Response) {
    const { term, limit, offset } = req.query;

    const products = await Product.scope({
      method: ['paginated', (term || ''), parseInt(limit || 10), parseInt(offset || 0)]
    }).findAndCount({
      distinct: true,
      include: [
        Category,
        Image
      ]
    });

    res.json({
      ...products,
      limit,
      offset
    })
  }
}

export default new ProductController();