import express from 'express';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';

import sequelize from './connector/sequelize'
import productRoutes from './routes/products';
import cors from 'cors';
import helmet from 'helmet';

import {
  Image,
  Product,
  Category,
  ProductImage,
  ProductCategory
} from './models'

dotenv.config();

sequelize.addModels([
  Image,
  Product,
  Category,
  ProductImage,
  ProductCategory
]);

const app = express();

app.use(helmet())
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set("port", process.env.PORT || 3000);

app.get("/products", productRoutes);

export default app;
export const server = app.listen(app.get("port"), () => {
  console.log(
    "  App is running at http://localhost:%d in %s mode",
    app.get("port"),
    app.get("env")
  );
  console.log("  Press CTRL-C to stop\n");
});
