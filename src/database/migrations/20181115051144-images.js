'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.createTable('images', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        name: {
          allowNull: false,
          type: Sequelize.STRING
        },
        path: {
          allowNull: false,
          type: Sequelize.STRING
        },
        created_at: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updated_at: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
      queryInterface.createTable('product_has_image', {
        product_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'products',
            key: 'id'
          },
          allowNull: false
        },
        image_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'images',
            key: 'id'
          },
          allowNull: false
        }
      }),
    ]);
    
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.dropTable('images'),
      queryInterface.dropTable('product_has_image')
    ]);
  }
};
