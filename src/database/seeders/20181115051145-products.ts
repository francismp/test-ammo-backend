import faker from 'faker';
import {
  Image,
  Product,
  Category,
  ProductImage,
  ProductCategory
} from '../../models';

import sequelize from '../../connector/sequelize'

sequelize.addModels([
  Image,
  Product,
  Category,
  ProductImage,
  ProductCategory
]);

function imageFactory() {
  return Image.create({
    name: faker.system.fileName('jpg'),
    path: faker.image.imageUrl(600, 400)
  });
}

function categoryFactory() {
  return Category.create({
    name: faker.commerce.productMaterial(),
    description: faker.lorem.lines(1)
  });
}

function productFactory() {
  return Product.create({
    sku: faker.finance.iban(),
    name: faker.commerce.productName(),
    price: faker.commerce.price(100, 250),
    old_price: faker.commerce.price(250, 500),
    description: faker.lorem.lines(1),
  });
}

exports.up = async () => {
  for (let i = 0; i < 150; i++) {
    const product = await productFactory();
  
    await product.$add('categories', (await categoryFactory()));
    await product.$add('images', (await imageFactory()));
    await product.$add('images', (await imageFactory()));
    await product.$add('images', (await imageFactory()));
    await product.$add('images', (await imageFactory()));
  }
};