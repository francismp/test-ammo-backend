## Available Scripts
In the project directory, you can run:

### `npm run database:setup`
Runs the `npm run migrate` and  `npm run seed` scripts (which you can run separately)

### `npm run build`
Builds the typescript generating a `dist` folder.

### `npm start`
Runs nodemon to execute the `.js` files from the dist folder.

### `npm run build:serve`
Runs `npm run build` and `npm start`.

### `npm run watch-ts`
Watches for typescript changes.

### `npm test`
Runs tests.

### Test URL
Backend: https://polar-refuge-61217.herokuapp.com/products
Frontend: https://test-ammo.firebaseapp.com